;;----------------------------------------------------------------------------------------------------------------------
;; Basic Forth dictionary
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; Header of dictionary:

docode          equ     0
last_link       defl    0

;;----------------------------------------------------------------------------------------------------------------------

head            macro label,size,name,action
                dw      last_link
                db      0
last_link       defl    $
                db      size
                db      name
label:          if action != 0 
                call    action
                endif
                endm

;;----------------------------------------------------------------------------------------------------------------------

bhead           macro label,size,name,action
                dw      last_link
                db      0
last_link       defl    $
                db      size
                db      name
label:          if action != 0
                break
                call    action
                endif
                endm

;;----------------------------------------------------------------------------------------------------------------------

immed           macro label,size,name
                dw      last_link
                db      1
last_link       defl    $
                db      size
                db      name
label:          call    ENTER
                endm

;;----------------------------------------------------------------------------------------------------------------------

cimmed          macro label,size,name
                dw      last_link
                db      1
last_link       defl    $
                db      size
                db      name
label:          call    docode
                endm

;;----------------------------------------------------------------------------------------------------------------------

bimmed          macro label,size,name
                dw      last_link
                db      1
last_link       defl    $
                db      size
                db      name
label:          break
                call    ENTER
                endm

;;----------------------------------------------------------------------------------------------------------------------

const           macro label,size,name,value
                head    label,size,name,docon
                dw      value
                endm

;;----------------------------------------------------------------------------------------------------------------------

user            macro label,size,name,value
                head    label,size,name,douser
                dw      value
                endm

;;----------------------------------------------------------------------------------------------------------------------

var             macro label,size,name
                head    label,size,name,dovar
                dw      0
                endm

;;----------------------------------------------------------------------------------------------------------------------

colon           macro label,size,name
                head   label,size,name,ENTER
                endm

;;----------------------------------------------------------------------------------------------------------------------

bcolon          macro label,size,name
                bhead   label,size,name,ENTER
                endm

;;----------------------------------------------------------------------------------------------------------------------
;; NEXT routines

nexthl          macro
                ld      e,(hl)
                inc     hl
                ld      d,(hl)
                inc     hl              ; (IP) -> W, IP+2 -> IP
                ex      de,hl           ; DE = IP, HL = W (from call)
                jp      (hl)            ; Next word
                endm

next            macro
                ex      de,hl
                nexthl
                endm

;;----------------------------------------------------------------------------------------------------------------------
;; Registers:
;;
;;      BC: Forth TOS
;;      HL: W register
;;      DE: IP register 
;;      SP: Parameter stack 
;;      IX: Return stack 

ENTER:
                ; This routine needs to be called
                dec     ix
                ld      (ix+0),d
                dec     ix
                ld      (ix+0),e
                pop     hl
                nexthl

;;----------------------------------------------------------------------------------------------------------------------
;; exit         --
                
                head    EXIT,4,"exit",docode
doexit:
                ld      e,(ix+0)
                inc     ix
                ld      d,(ix+0)
                inc     ix              ; POP IP
                next

;;----------------------------------------------------------------------------------------------------------------------
;; lit          -- x

                head    LIT,3,"lit",docode

                push    bc              ; push old TOS
                ld      a,(de)          ; fetch cell at IP to TOS
                ld      c,a
                inc     de
                ld      a,(de)
                ld      b,a
                inc     de
                next

;;----------------------------------------------------------------------------------------------------------------------
;; execute      i*x xt -- j*x           Execute forth word at xt that has i parameters and j resuls

                head    EXECUTE,7,"execute",docode

                ld      h,b
                ld      l,c             ; HL = address of word
                pop     bc              ; get new TOS
                jp      (hl)

;;----------------------------------------------------------------------------------------------------------------------
;; variable     --                      Define a forth variable (VARIABLE <name>)
;;
;;      CREATE 1 CELLS ALLOT ;

                colon   VARIABLE,8,"variable"

                dw      CREATE,LIT,1,CELLS,ALLOT,EXIT

docreate:
dovar:          pop     hl              ; HL = PFA (from call)
                push    bc              ; Push old TOS
                ld      b,h
                ld      c,l             ; TOS = PFA
                next

;;----------------------------------------------------------------------------------------------------------------------
;; constant     n --                    Define a forth constant (<value> CONSTANT <name>)
;;
;;      CREATE , DOES> (machine code fragment)

                colon   CONSTANT,8,"constant"

                dw      CREATE,COMMA,XDOES

docon:          pop     hl              ; HL = PFA
                push    bc              ; Push old TOS
                ld      c,(hl)
                inc     hl
                ld      b,(hl)          ; Push contents of PFA
                next
;;----------------------------------------------------------------------------------------------------------------------
;; user         n --                    Define a user variable 'n'
;;
;;      CREATE , DOES>

                colon   USER,4,"user"

                dw      CREATE,COMMA,XDOES

douser:         pop     hl              ; HL = PFA
                push    bc              ; Push old TOS
                ld      c,(hl)          ; Push contents of PFA
                inc     hl
                ld      b,(hl)
                push    iy              ; Copy user base address to HL
                pop     hl
                add     hl,bc           ; HL = address of user variable
                ld      b,h
                ld      c,l             ; Push address of user variable
                next

;;----------------------------------------------------------------------------------------------------------------------
;; dodoes       Code action of DOES> clause
;; entered by:  CALL fragment
;;              PFA
;;                ...
;;              fragment:
;;              CALL dodoes
;;              high-level thread

dodoes:         dec     ix              ; Push old IP on ret stack
                ld      (ix+0),d
                dec     ix
                ld      (ix+1),e
                pop     de              ; Address of new thread ->IP
                pop     hl              ; Address of PFA
                push    bc
                ld      b,h
                ld      c,l             ; push PFA
                next

;;----------------------------------------------------------------------------------------------------------------------
;; bye          --                      Quits forth

                head    BYE,3,"bye",docode

                jp      endForth

;;----------------------------------------------------------------------------------------------------------------------
;; emit         c --                    Output a character to console

                head    EMIT,4,"emit",docode

                ld      a,c
                pop     bc              ; Get new TOS
                call    printChar
                next

;;----------------------------------------------------------------------------------------------------------------------
;; key?         -- f                    Return true if there is a key available

                head    KEYQ,4,"key?",docode

                push    bc
                ld      hl,KFlags
                bit     0,(hl)
                jr      z,.no_key

                ld      bc,1
                jr      .end

.no_key         ld      bc,0
.end            next

;;----------------------------------------------------------------------------------------------------------------------
;; key          -- c                    Fetch the key code (will wait if no key)

                head    KEY,3,"key",docode

.no_key         call    consoleUpdate
                ld      hl,KFlags
                bit     0,(hl)
                jr      z,.no_key

                ld      a,(Key)
                push    bc
                ld      c,a
                ld      b,0
                res     0,(hl)
                next

;;----------------------------------------------------------------------------------------------------------------------
;; dup          x -- x x                Duplicate the top value on the stack

                head    DUP,3,"dup",docode

pushtos:
                push    bc
                next

;;----------------------------------------------------------------------------------------------------------------------
;; ?dup         x -- 0 | x x            Duplicate if nonzero

                head    QDUP,4,"?dup",docode

                ld      a,b
                or      c
                jr      nz,pushtos
                next

;;----------------------------------------------------------------------------------------------------------------------
;; drop         x --                    Drop top of stack

                head    DROP,4,"drop",docode

poptos:         pop     bc
                next

;;----------------------------------------------------------------------------------------------------------------------
;; swap         x1 x2 -- x2 x1          Swap top two items

                head    SWAP,4,"swap",docode

                pop     hl              ; HL = x1
                push    bc              ; Stack: x2 (x2)
                ld      b,h
                ld      c,l             ; Stack: x2 (x1)
                next

;;----------------------------------------------------------------------------------------------------------------------
;; over         x1 x2 -- x1 x2 x1       Duplicate 2nd item on stack

                head    OVER,4,"over",docode

                pop     hl              ; HL = x1
                push    hl              ; Stack: x1 (x2)
                push    bc              ; Stack: x1 x2 (x2)
                ld      b,h
                ld      c,l             ; Stack: x1 x2 (x1)
                next

;;----------------------------------------------------------------------------------------------------------------------
;; rot          x1 x2 x3 -- x2 x3 x1    Bring the 3rd item to the top of the stack

                head    ROT,3,"rot",docode

                pop     hl              ; HL = x2
                ex      (sp),hl         ; HL = x1, Stack: x2 (x3)
                push    bc              ; Stack: x2 x3 (x3)
                ld      b,h
                ld      c,l             ; Stack: x2 x3 (x1)
                next

;;----------------------------------------------------------------------------------------------------------------------
;; nip          x1 x2 -- x2             Remove 2nd element of stack
;;
;;      swap drop ;

                colon   NIP,3,"nip"

                dw      SWAP,DROP,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; tuck         x1 x2 -- x2 x1 x2       Put TOS behind 2nd
;;
;;      swap over ;

                colon   TUCK,4,"tuck"

                dw      SWAP,OVER,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; >r           x --    R: -- x         Push to return stack

                head    TOR,2,">r",docode

                dec     ix              ; Push TOS to return stack
                ld      (ix+0),b
                dec     ix
                ld      (ix+0),c
                pop     bc              ; New TOS
                next

;;----------------------------------------------------------------------------------------------------------------------
;; r>           -- x    R: x --         Pop from return stack

                head    RFROM,2,"r>",docode

                push    bc              ; Push old TOS
                ld      c,(ix+0)
                inc     ix
                ld      b,(ix+0)
                inc     ix              ; Pop from RS and push to PS
                next

;;----------------------------------------------------------------------------------------------------------------------
;; r@           -- x    R: x -- x       Fetch from rtn stack

                head    RFETCH,2,"r@",docode

                push    bc              ; Push old TOS
                ld      c,(ix+0)
                ld      b,(ix+1)
                next

;;----------------------------------------------------------------------------------------------------------------------
;; sp@          -- a-addr               Get data stack pointer

                head    SPFETCH,3,"sp@",docode

                push    bc              ; New TOS
                ld      hl,0
                add     hl,sp           ; SP -> HL
                ld      b,h
                ld      c,l             ; Push SP
                next

;;----------------------------------------------------------------------------------------------------------------------
;; sp!          a-addr --               Set data stack pointer

                head    SPSTORE,3,"sp!",docode

                ld      h,b
                ld      l,c
                ld      sp,hl
                pop     bc
                next

;;----------------------------------------------------------------------------------------------------------------------
;; rp@          -- a-addr               Get return stack pointer

                head    RPFETCH,3,"rp@",docode

                push    bc
                push    ix
                pop     bc
                next

;;----------------------------------------------------------------------------------------------------------------------
;; rp!          a-addr --               Set return stack pointer

                head    RPSTORE,3,"rp!",docode

                push    bc
                pop     ix              ; TOS -> RP
                pop     bc
                next

;;----------------------------------------------------------------------------------------------------------------------
;; !            x addr --               Store cell in memory

                head    STORE,1,"!",docode

                ld      h,b
                ld      l,c             ; HL = address
                pop     bc              ; BC = value
                ld      (hl),c
                inc     hl
                ld      (hl),b          ; (HL) = BC
                pop     bc              ; Drop value
                next

;;----------------------------------------------------------------------------------------------------------------------
;; c!           c addr --               Store character in memory

                head    CSTORE,2,"c!",docode

                ld      h,b
                ld      l,c             ; HL = address
                pop     bc              ; BC = value
                ld      (hl),c
                pop     bc
                next

;;----------------------------------------------------------------------------------------------------------------------
;; @            addr -- x               Fetch cell from memory

                head    FETCH,1,"@",docode

                ld      h,b
                ld      l,c             ; HL = address
                ld      c,(hl)
                inc     hl
                ld      b,(hl)
                next

;;----------------------------------------------------------------------------------------------------------------------
;; c@           addr -- c               Fetch character from memory

                head    CFETCH,2,"c@",docode

                ld      a,(bc)
                ld      c,a
                ld      b,0
                next

;;----------------------------------------------------------------------------------------------------------------------
;; pc!          c addr --               Output char to port

                head    PCSTORE,3,"pc!",docode

                pop     hl              ; HL = character
                out     (c),l           ; Output character to port BC
                pop     bc
                next

;;----------------------------------------------------------------------------------------------------------------------
;; pc@          addr -- c               Input char from port

                head    PCFETCH,3,"pc@",docode

                in      c,(c)
                ld      b,0             ; pop addr, push value from port BC
                next

;;----------------------------------------------------------------------------------------------------------------------
;; reg!         value register --       Send byte to register

                head    REGSTORE,4,"reg!",docode

                ld      a,c
                ld      (__nr+2),a      ; Use SMC to change register value
                pop     hl              ; L = value
                ld      a,l             ; A
__nr            nextreg $00,a
                pop     bc
                next

;;----------------------------------------------------------------------------------------------------------------------
;; port@        port -- value           Read from a IO port.

                head    PORTFETCH,5,"port@",docode

                in      c,(c)
                ld      b,0
                next

;;----------------------------------------------------------------------------------------------------------------------
;; port!        value port --           Write value to IO port.

                head    PORTSTORE,5,"port!",docode

                pop     hl
                out     (c),l
                pop     bc
                next

;;----------------------------------------------------------------------------------------------------------------------
;; +            n1/u1 n2/u2 -- n3/u3    n3 = n1 + n2

                head    PLUS,1,"+",docode

                pop     hl              ; HL = n1
                add     hl,bc           ; HL = n1 + n2
                ld      b,h
                ld      c,l             ; Push it on stack
                next

;;----------------------------------------------------------------------------------------------------------------------
;; m+           d n -- d                ; Add single to double

                head    MPLUS,2,"m+",docode

                ex      de,hl           ; HL = IP
                pop     de              ; DE = dh
                ex      (sp),hl         ; HL = dl, save IP, Stack: IP (n)
                add     hl,bc           ; HL = dl + n
                ld      b,d
                ld      c,e             ; BC = hi cell, Stack: IP (dh)
                jr      nc,.no_carry
                inc     bc              ; Handle the carry
.no_carry       pop     de              ; Restore IP, Stack: (dh)
                push    hl              ; Stack: dl (dh)
                next

;;----------------------------------------------------------------------------------------------------------------------
;; -            n1/u1 n2/u2 -- n3/u3    n3 = n1 - n2

                head    MINUS,1,"-",docode

                pop     hl              ; HL = n1
                and     a               ; Clear CF
                sbc     hl,bc           ; HL = n1 - n2
                ld      b,h
                ld      c,l
                next

;;----------------------------------------------------------------------------------------------------------------------
;; and          x1 x2 -- x3             x3 = x1 & x2

                head    AND,3,"and",docode

                pop     hl              ; HL = x1
                ld      a,b
                and     h
                ld      b,a             ; B = B & H
                ld      a,c
                and     l
                ld      c,a             ; C = C & L
                next

;;----------------------------------------------------------------------------------------------------------------------
;; or           x1 x2 -- x3             x3 = x1 | x2

                head    OR,2,"or",docode

                pop     hl              ; HL = x1
                ld      a,b
                or      h
                ld      b,a             ; B = B | H
                ld      a,c
                or      l
                ld      c,a             ; C = C | L
                next

;;----------------------------------------------------------------------------------------------------------------------
;; xor          x1 x2 -- x3             x3 = x1 ^ x2

                head    XOR,3,"xor",docode

                pop     hl              ; HL = x1
                ld      a,b
                xor     h
                ld      b,a             ; B = B ^ H
                ld      a,c
                xor     l
                ld      c,a             ; C = C ^ L
                next

;;----------------------------------------------------------------------------------------------------------------------
;; invert       x1 -- x2                x1 = ~x2

                head    INVERT,6,"invert",docode

                ld      a,b
                cpl
                ld      b,a
                ld      a,c
                cpl
                ld      c,a
                next

;;----------------------------------------------------------------------------------------------------------------------
;; negate       x1 -- x2                x1 = -x2

                head    NEGATE,6,"negate",docode

                ld      a,b
                cpl
                ld      b,a
                ld      a,c
                cpl
                ld      c,a
                inc     bc
                next

;;----------------------------------------------------------------------------------------------------------------------
;; 1+           n1/u1 -- n2/u2          n2 = n1 + 1

                head    ONEPLUS,2,"1+",docode

                inc     bc
                next

;;----------------------------------------------------------------------------------------------------------------------
;; 1-           n1/u1 -- n2/u2          n2 = n1 - 1

                head    ONEMINUS,2,"1-",docode

                dec     bc
                next

;;----------------------------------------------------------------------------------------------------------------------
;; ><           x1 -- x2                Swap bytes

                head    SWAPBYTES,2,"><",docode

                ld      a,b
                ld      b,c
                ld      c,a
                next

;;----------------------------------------------------------------------------------------------------------------------
;; 2*           x1 -- x2                Arithmetic left shift

                head    TWOSTAR,2,"2*",docode

                sla     c
                rl      b
                next

;;----------------------------------------------------------------------------------------------------------------------
;; 2/           x1 -- x2                Arithmetic right shift

                head    TWOSLASH,2,"2/",docode

                sra     b
                rr      c
                next

;;----------------------------------------------------------------------------------------------------------------------
;; LSHIFT       x1 u -- x2              x2 = x1 << u (make sure u < 32)

                head    LSHIFT,6,"lshift",docode

                ld      b,c             ; B = number of places
                pop     hl
                ex      de,hl           ; DE = x1, HL = IP
                bsla    de,b            ; DE = x1 << u
                ex      de,hl
                ld      b,h
                ld      c,l
                next

;;----------------------------------------------------------------------------------------------------------------------
;; RSHIFT       x1 u -- x2              x2 = x1 >> u (make sure u < 32)

                head    RSHIFT,6,"rshift",docode

                ld      b,c             ; B = number of places
                pop     hl
                ex      de,hl           ; DE = x1, HL = IP
                bsrl    de,b            ; DE = x1 >> u
                ex      de,hl
                ld      b,h
                ld      c,l
                next

;;----------------------------------------------------------------------------------------------------------------------
;; +!           n/u addr --             *p = *p + n

                head    PLUSSTORE,2,"+!",docode

                pop     hl              ; HL = n
                ld      a,(bc)          ; lo byte
                add     a,l
                ld      (bc),a
                inc     bc
                ld      a,(bc)
                adc     a,h
                ld      (bc),a
                pop     bc
                next

;;----------------------------------------------------------------------------------------------------------------------
;; 0=           n/u -- flag             Return true if TOS = 0

                head    ZEROEQUAL,2,"0=",docode

                ld      a,b
                or      c               ; Result = 0 if BC was 0
                sub     1               ; CF set if BC was 0
                sbc     a,a             ; Propagate CF through A
                ld      b,a
                ld      c,a             ; Put $0000 or $ffff in TOS
                next

;;----------------------------------------------------------------------------------------------------------------------
;; 0<           n -- flag               Return true if TOS is negative

                head    ZEROLESS,2,"0<",docode

                sla     b               ; Sign but -> CF flag
                sbc     a,a             ; Propagate CF through A 
                ld      b,a
                ld      c,a
                next

;;----------------------------------------------------------------------------------------------------------------------
;; =            x1 x2 -- flag           Test x1 == x2

                head    EQUAL,1,"=",docode

                pop     hl              ; HL = x1
                and     a
                sbc     hl,bc           ; HL = x1 - x2
                jr      z,tostrue
tosfalse:       ld      bc,0
                next

;;----------------------------------------------------------------------------------------------------------------------
;; <>           x1 x2 -- flag           Test x1 != x2
;;
;;      = 0= ;

                colon   NOTEQUAL,2,"<>"

                dw      EQUAL,ZEROEQUAL,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; <            n1 n2 -- flag           Test n1 < n2, signed

                head    LESS,1,"<",docode

                pop     hl              ; HL = n1
                and     a               ; Clear carry flag
                sbc     hl,bc           ; HL = n1 - n2

                ; If result is -ve & not OV, n1 < n2
                ; N & OV => n1 +ve, n2 -ve, result -ve, so n1 > n2
                ; If result is +ve & not OV, n1 >= n2
                ; !N & OV => n1 -ve, n2 +ve, result +ve, so n1 < n2 this OV reverses the sense of the sign bit
                jp      pe,revsense     ; If OV, use rev. sense
                jp      p,tosfalse      ; If +ve, result false
tostrue:        ld      bc,$ffff
                next
revsense:       jp      m,tosfalse      ; OV: if -ve, result false
                jr      tostrue         ;     if +ve, result true

;;----------------------------------------------------------------------------------------------------------------------
;; >            n1 n2 -- flag           Test n1 > n2, signed
;;
;;      swap < ;

                colon   GREATER,1,">"

                dw      SWAP,LESS,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; U<           u1 u2 -- flag           Test u1 < u2, unsigned

                head    ULESS,2,"u<",docode

                pop     hl              ; HL = u1
                and     a               ; Clear CF
                sbc     hl,bc           ; HL = u1 - u2
                sbc     a,a             ; Propagate CF through A
                ld      b,a
                ld      c,a
                next

;;----------------------------------------------------------------------------------------------------------------------
;; U>           u1 u2 -- flag           Test u1 > u2, unsigned
;;
;;      swap u< ;

                colon   UGREATER,2,"u>"

                dw      SWAP,ULESS,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; branch       --                      Branch always

                head    BRANCH,6,"branch",docode

dobranch:       ld      a,(de)          ; Get inline value -> IP
                ld      l,a
                inc     de
                ld      a,(de)
                ld      h,a             ; HL = new IP
                nexthl

;;----------------------------------------------------------------------------------------------------------------------
;; ?branch      x --                    Branch if TOS zero

                head    QBRANCH,7,"?branch",docode

                ld      a,b
                or      c
                pop     bc
                jr      z,dobranch      ; Branch if zero
                inc     de
                inc     de              ; Otherwise, skip inline value
                next

;;----------------------------------------------------------------------------------------------------------------------
;; (do)         n1|u1 n2|u2 --          R: -- sys1 sys2         (Runtime code for DO)

                head    XDO,4,"(do)",docode

                ex      de,hl           ; HL = IP
                ex      (sp),hl         ; HL = n1 (limit), store SP, Stack: IP (n2)
                ex      de,hl           ; DE = n1 (limit)
                ld      hl,$8000
                and     a
                sbc     hl,de           ; Make limit == $8000
                dec     ix              ; Push this fudge factor
                ld      (ix+0),h
                dec     ix
                ld      (ix+0),l
                add     hl,bc           ; Add fudge to start value
                dec     ix              ; Push adjusted start value
                ld      (ix+0),h
                dec     ix
                ld      (ix+0),l
                pop     de              ; Restore saved IP
                pop     bc              ; Drop value
                next

;;----------------------------------------------------------------------------------------------------------------------
;; (loop)       R: sys1 sys2 --                                 (Runtime code for LOOP)
;;
;; Add 1 to the loop index.  If loop terminates, clean up the resturn stack and skip the branch.  Note that
;; loop terminates when index = $8000

                head    XLOOP,6,"(loop)",docode

                exx
                ld      bc,1
.looptst        ld      l,(ix+0)
                ld      h,(ix+1)        ; Get loop index
                and     a
                adc     hl,bc           ; Increment with overflow test
                jp      pe,.loopterm

                ; Continue loop
                ld      (ix+0),l
                ld      (ix+1),h        ; Save the updated index
                exx
                jr      dobranch

.loopterm       ld      bc,4            ; Discard the loop info
                add     ix,bc
                exx
                inc     de
                inc     de              ; Skip the inline branch
                next

;;----------------------------------------------------------------------------------------------------------------------
;; (+loop)      n --    R: sys1 sys2 --                         (Runtime code for +LOOP)
;;
;; Add n to the loop index.

                head    XPLUSLOOP,7,"(+loop)",docode

                pop     hl              ; HL = new TOS
                push    bc
                ld      b,h
                ld      c,l
                exx
                pop     bc
                jr      XLOOP.looptst

;;----------------------------------------------------------------------------------------------------------------------
;; i            -- n                    Get loop index

                head    II,1,"i",docode

                push    bc              ; Push old TOS
                ld      l,(ix+0)
                ld      h,(ix+1)        ; HL = current loop index
                ld      c,(ix+2)
                ld      b,(ix+3)        ; BC = fudge factor
                and     a
                sbc     hl,bc           ; HL = index
                ld      b,h
                ld      c,l
                next

;;----------------------------------------------------------------------------------------------------------------------
;; j            -- n                    Get 2nd loop index

                head    JJ,1,"j",docode

                push    bc              ; Push old TOS
                ld      l,(ix+4)
                ld      h,(ix+5)        ; HL = current loop index
                ld      c,(ix+6)
                ld      b,(ix+7)        ; BC = fudge factor
                and     a
                sbc     hl,bc           ; HL = index
                ld      b,h
                ld      c,l
                next

;;----------------------------------------------------------------------------------------------------------------------
;; UNLOOP       --      R: sys1 sys2 --         Drop loop parameters

                head    UNLOOP,6,"unloop",docode
                inc     ix
                inc     ix
                inc     ix
                inc     ix
                next

;;----------------------------------------------------------------------------------------------------------------------
