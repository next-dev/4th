;;----------------------------------------------------------------------------------------------------------------------
;; 2nd part of dictionary
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; um*          u1 u2 -- ud             Unsigned multiply

                head    UMSTAR,3,"um*",docode

                push    bc              ; Stack u1 u2 (u2)
                exx
                pop     bc              ; BC = u2, Stack: u1 (u2)
                pop     de              ; DE = u1, Stack: (u2)
                ld      hl,0             ; Result will be in HLDE
                ld      a,17            ; Loop counter
                and     a               ; Clear CF
.loop           rr      h
                rr      l
                rr      d
                rr      e
                jr      nc,.no_add
                add     hl,bc
.no_add         dec     a
                jr      nz,.loop
                push    de              ; Store HLDE, Stack: de (u2)
                push    hl              ; Stack: de hl (u2)
                exx
                pop     bc              ; Stack: de (hl)
                next

;;----------------------------------------------------------------------------------------------------------------------
;; um/mod       ud u1 -- u2 u3          u2 = ud % u1, u3 = ud / u1

                head    UMSLASHMOD,6,"um/mod",docode

                push    bc              ; Stack: dl dh (u1)
                exx
                pop     bc              ; Stack: dl dh (u1)
                pop     hl              ; HLDE = dividend
                pop     de              ; Stack: (u1)

                ld      a,16            ; loop counter
                sla     e
                rl      d               ; MSB DE -> CF
.loop           adc     hl,hl
                jr      nc,.udiv3

                ; Case 1: 17-bit, CF:HL = 1xxxx...
                and     a
                sbc     hl,bc
                and     a
                jr      .udiv4

                ; Case 2: 16-bit CF:HL = 0xxxx....
.udiv3          sbc     hl,bc           ; Try subtract
                jr      nc,.udiv4       ; If CF == 0, subtract OK
                add     hl,bc           ; Cancel subtract
                scf
.udiv4          rl      e
                rl      d               ; Rotate CF into DE and MSB DE -> CF
                dec     a
                jr      nz,.loop

                ; We have complemented quotient in DE and remainder in HL
                ld      a,d
                cpl
                ld      b,a
                ld      a,e
                cpl     
                ld      c,a             ; Push quotient
                push    hl              ; Push remainder

                push    bc
                exx
                pop     bc              ; BC' -> BC
                next

;;----------------------------------------------------------------------------------------------------------------------
;; fill         addr u char --          Fill memory with char

                head    FILL,4,"fill",docode

                ld      a,c             ; A = char
                pop     bc              ; BC = count
                pop     hl              ; HL = address
                call    memfill
                pop     bc              ; Fill TOS
                next

;;----------------------------------------------------------------------------------------------------------------------
;; cmove        addrs addrd u --        Copy u bytes from addr1 to addr2

                head    CMOVE,5,"cmove",docode

                push    bc
                exx
                pop     bc              ; BC = count
                pop     de              ; DE = destination address
                pop     hl              ; HL = source address
                call    memcpy
                exx
                pop     bc
                next

;;----------------------------------------------------------------------------------------------------------------------
;; todo: DMA-based cmove>
;;
;; cmove>       addrs addrd u --        Copy u bytes from end to start

                head    CMOVEUP,6,"cmove>",docode

                push    bc
                exx
                pop     bc              ; BC = count
                pop     hl              ; Destination address
                pop     de              ; Source address
                ld      a,b
                or      c
                jr      z,.done

                add     hl,bc           ; Last byte in destination
                dec     hl
                ex      de,hl
                add     hl,bc           ; Last byte in source
                dec     hl
                lddr

.done           exx
                pop     bc
                next

;;----------------------------------------------------------------------------------------------------------------------
;; skip         addr u c -- addr u      Skip matching characters.  Returns address and count

                head    SKIP,4,"skip",docode

                ld      a,c
                exx
                pop     bc              ; BC = count
                pop     hl              ; Address
                ld      e,a             ; E = character
                ld      a,b
                or      c               ; count == 0?
                jr      z,.done

                ld      a,e
.loop           cpi
                jr      nz,.exit        ; Character mismatch -> exit
                jp      pe,.loop        ; Count not exhausted
                jr      .done           ; Count 0 -> no mismatch
.exit           inc     bc              ; Undo last to point at mismatch char
                dec     hl
.done           push    hl              ; Push address of first mismatch
                push    bc              ; Update count
                exx
                pop     bc
                next

;;----------------------------------------------------------------------------------------------------------------------
;; scan         addr u c -- addr u      Find matching char

                head    SCAN,4,"scan",docode

                ld      a,c             ; Scan character
                exx
                pop     bc              ; BC = count
                pop     hl              ; HL = address
                ld      e,a
                ld      a,b
                or      c
                jr      z,.done

                ld      a,e
                cpir                    ; Scan until match found or count = 0;
                jr      nz,.done

                inc     bc
                dec     hl              ; Point at match char

.done           push    hl              ; Updated address
                push    bc              ; Updated count
                exx
                pop     bc
                next

;;----------------------------------------------------------------------------------------------------------------------
;; s=           addr1 addr2 u -- n      String compare

                head    SEQUAL,2,"s=",docode

                push    bc
                exx
                pop     bc              ; BC = count
                pop     hl              ; HL = addr2
                pop     de              ; DE = addr1

                ld      a,b
                or      c
                jr      z,.match        ; Match!
.loop           ld      a,(de)
                inc     de
                cpi
                jr      nz,.diff        ; Char mismatch!
                jp      pe,.loop

.match          exx
                ld      bc,0            ; Match
                jr      .next

.diff           dec     hl              ; Undo last cpi instruction
                cp      (hl)            ; Set CF if c1 < c2
                sbc     a,a
                exx
                ld      b,a
                or      1
                ld      c,a             ; BC = -1 or +1 based on CF
.next           next

;;----------------------------------------------------------------------------------------------------------------------
;; align        --                      Align here

                head    ALIGN,5,"align",docode

noop:           next

;;----------------------------------------------------------------------------------------------------------------------
;; aligned      addr -- aligned-addr    Align given address

                head    ALIGNED,7,"aligned",docode

                jr      noop

;;----------------------------------------------------------------------------------------------------------------------
;; cell         -- n                    Size of 1 cell

                const   CELL,4,"cell",2

;;----------------------------------------------------------------------------------------------------------------------
;; cell+        addr -- addr            Add cell size to address

                head    CELLPLUS,5,"cell+",docode

                inc     bc
                inc     bc
                next

;;----------------------------------------------------------------------------------------------------------------------
;; cells        n1 -- n2                Convert cells to bytes

                head    CELLS,5,"cells",docode

                jp      TWOSTAR

;;----------------------------------------------------------------------------------------------------------------------
;; char+        addr - addr             Add char size to address

                head    CHARPLUS,5,"char+",docode

                jp      ONEPLUS

;;----------------------------------------------------------------------------------------------------------------------
;; chars        n1 -- n2                Convert cells to bytes

                head    CHARS,5,"chars",docode

                jr      noop

;;----------------------------------------------------------------------------------------------------------------------
;; >body        xt -- addfr             Address of PFA from CFA
;;
;;      3 + ;

                colon   TOBODY,5,">body"

                dw      LIT,3,PLUS,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; compile,     xt --                   Append execution token

                head    COMMAXT,8,"compile,",docode

                jp      COMMA

;;----------------------------------------------------------------------------------------------------------------------
;; !cf          addr cfa --             Set code action of a word
;;
;;      $CD over c!                     Store 'CALL addr' instruction
;;      1+ ! ;

                colon   STORECF,3,"!cf"

                dw      LIT,$cd,OVER,CSTORE,ONEPLUS,STORE,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; ,cf          addr --                 Append a code field
;;
;;      here !cf 3 allot ;

                colon   COMMACF,3,",cf"

                dw      HERE,STORECF,LIT,3,ALLOT,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; !colon       --                      Change code field to docolon
;;
;;      -3 allot <docolon> ,CF

                colon   STORECOLON,6,"!colon"

                dw      LIT,-3,ALLOT,LIT,ENTER,COMMACF,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; ,exit        --                      Append hi-level EXIT action
;;
;;      ['] EXIT ,XT ;
;;

                colon   COMMAEXIT,5,",exit"

                dw      LIT,EXIT,COMMAXT,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; ,branch      xt --                   Append a branch instruction

                head    COMMABRANCH,7,",branch",docode

                jp      COMMA

;;----------------------------------------------------------------------------------------------------------------------
;; ,dest        dest --                 Append a branch address

                head    COMMADEST,5,",dest",docode

                jp      COMMA

;;----------------------------------------------------------------------------------------------------------------------
;; !dest        dest addr --            Change a branch destination

                head    STOREDEST,5,"!dest",docode

                jp      STORE

;;----------------------------------------------------------------------------------------------------------------------
;; bl           -- char                 An ASCII space

                const   BL,2,"bl",$20

;;----------------------------------------------------------------------------------------------------------------------
;; tibsize      -- n                    Size of TIB

                const   TIBSIZE,7,"tibsize",126

;;----------------------------------------------------------------------------------------------------------------------
;; tib          -- addr                 Terminal Input Buffer

                const   TIB,3,"tib",TibBuffer

;;----------------------------------------------------------------------------------------------------------------------
;; u0           -- addr                 Current user area

                user    U0,2,"u0",0

;;----------------------------------------------------------------------------------------------------------------------
;; >in          -- addr                 Holds offset into TIB

                user    TOIN,3,">in",2

;;----------------------------------------------------------------------------------------------------------------------
;; base         -- addr                 Holds conversion radix

                user    BASE,4,"base",4

;;----------------------------------------------------------------------------------------------------------------------
;; state        -- addr                 Holds compiler state

                user    STATE,5,"state",6

;;----------------------------------------------------------------------------------------------------------------------
;; dp           -- addr                 Holds dictionary pointer

                user    DP,2,"dp",8

;;----------------------------------------------------------------------------------------------------------------------
;; 'source      -- addr                 Two cells: len, addrs

                user    TICKSOURCE,7,"'source",10

;;----------------------------------------------------------------------------------------------------------------------
;; latest       -- addr                 Last word in dictionary

                user    LATEST,6,"latest",14

;;----------------------------------------------------------------------------------------------------------------------
;; hp           -- addr                 HOLD pointer

                user    HP,2,"hp",16

;;----------------------------------------------------------------------------------------------------------------------
;; lp           -- addr                 Leave-stack pointer

                user    LP,2,"lp",18

;;----------------------------------------------------------------------------------------------------------------------
;; s0           -- addr                 End of parameter stack

                const   S0,2,"s0",EndParamStack-2

;;----------------------------------------------------------------------------------------------------------------------
;; PAD          -- addr                 User PAD buffer

                const   PAD,3,"pad",PadBuffer

;;----------------------------------------------------------------------------------------------------------------------
;; L0           -- addr                 Bottom of Leave Stack

                const   L0,2,"l0",ReturnStack

;;----------------------------------------------------------------------------------------------------------------------
;; R0           -- addr                 Start of return stacl

                const   R0,2,"r0",ReturnStack+256

;;----------------------------------------------------------------------------------------------------------------------
;; uinit        -- addr                 Initial values for user area

                head    UINIT,5,"uinit",docreate

                dw      0,0,10,0        ; Reserved, >IN, BASE, STATE
                dw      $8000           ; DP
                dw      0,0             ; SOURCE init'd elsewhere
                dw      LASTWORD        ; LATEST
                dw      0               ; HP init'd elsewhere

;;----------------------------------------------------------------------------------------------------------------------
;; #init        -- n                    Num of bytes of user area init data

                const   NINIT,5,"#init",18

;;----------------------------------------------------------------------------------------------------------------------
;; s>d          n -- d                  Single -> double precision
;;
;;      dup 0< ;

                colon   STOD,3,"s>d"

                dw      DUP,ZEROLESS,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; ?negate      n1 n2 -- n3             Negate n1 if n2 is negative
;;
;;      0< if negate then ;

                colon   QNEGATE,7,"?negate"

                dw      ZEROLESS,QBRANCH,QNEG1,NEGATE
QNEG1:          dw      EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; abs          n1 -- +n2               Absolute value
;;
;;      dup ?negate ;

                colon   ABS,3,"abs"

                dw      DUP,QNEGATE,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; dnegate      d1 -- d2                Negate double precision
;;
;;      swap invert swap invert 1 m+ ;

                colon   DNEGATE,7,"dnegate"

                dw      SWAP,INVERT,SWAP,INVERT,LIT,1,MPLUS,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; ?dnegate     d1 n -- d2              Negate d1 is n is negative
;;
;;      0< if dnegate then ;

                colon   QDNEGATE,8,"?dnegate"

                dw      ZEROLESS,QBRANCH,DNEG1,DNEGATE
DNEG1:          dw      EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; dabs         d1 -- +d2               Absolute value with double precision
;;
;;      dup ?dnegate ;

                colon   DABS,4,"dabs"

                dw      DUP,QDNEGATE,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; m*           n1 n2 -- d              Signed 16*16 -> 32 multiply
;;
;;      2dup xor >r             Carries sign of result
;;      swap abs swap abs um*
;;      r> ?dnegate ;

                colon   MSTAR,2,"m*"

                dw      TWODUP,XOR,TOR
                dw      SWAP,ABS,SWAP,ABS,UMSTAR
                dw      RFROM,QDNEGATE,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; sm/rem       d1 n1 -- n2 n3          Symmetric signed divide
;;
;;      2dup xor >r             Sign of quotient
;;      over >r                 Sign of remainder
;;      abs >r dabs r> um/mod
;;      swap r> ?negate
;;      swap r> ?negate ;

                colon   SMSLASHREM,6,"sm/rem"

                dw      TWODUP,XOR,TOR,OVER,TOR
                dw      ABS,TOR,DABS,RFROM,UMSLASHMOD
                dw      SWAP,RFROM,QDNEGATE
                dw      SWAP,RFROM,QDNEGATE
                dw      EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; fm/mod       d1 n1 -- n2 n3          Floored signed div
;;
;;      dup >r
;;      sm/rem
;;      dup 0< if
;;              swap r> +
;;              swap 1-
;;      else r> drop then ;

                colon   FMSLASHMOD,6,"fm/mod"

                dw      DUP,TOR,SMSLASHREM
                dw      DUP,ZEROLESS,QBRANCH,FMMOD1
                dw      SWAP,RFROM,PLUS,SWAP,ONEMINUS
                dw      BRANCH,FMMOD2
FMMOD1:         dw      RFROM,DROP
FMMOD2:         dw      EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; *            n1 n2 -- n3             Signed multiply
;;
;;      m* drop ;

                colon   STAR,1,"*"

                dw      MSTAR,DROP,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; /mod         n1 n2 -- n3 n4          Signed divide/remainder (n3 remainder, n4 quotient)
;;
;;      >r s>d r> fm/mod ;

                colon   SLASHMOD,4,"/mod"

                dw      TOR,STOD,RFROM,FMSLASHMOD,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; */mod        n1 n2 n3 -- n4 n5       n1*n2/n3 (n4 remainder, n5 quotient)
;;
;;      >r m* r> fm/mod ;

                colon   SSMOD,5,"*/mod"

                dw      TOR,MSTAR,RFROM,FMSLASHMOD,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; */           n1 n2 n3 -- n4          n1*n2/n3
;;
;;      */mod nip ;

                colon   STARSLASH,2,"*/"

                dw      SSMOD,NIP,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; max          n1 n2 -- n3             Signed maximum
;;
;;      2dup < if swap then drop ;

                colon   MAX,3,"max"

                dw      TWODUP,LESS,QBRANCH,.l1,SWAP
.l1:            dw      DROP,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; min          n1 n2 -- n3             Signed minimum
;;
;;      2dup > if swap then drop ;

                colon   MIN,3,"min"

                dw      TWODUP,GREATER,QBRANCH,.l1,SWAP
.l1:            dw      DROP,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; 2@           addr -- x2 x1           Fetch 2 cells
;;
;;      dup cell+ @ swap @ ;

                colon   TWOFETCH,2,"2@"

                dw      DUP,CELLPLUS,FETCH,SWAP,FETCH,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; 2!           x1 x2 addr --           Store 2 cells
;;
;;      swap over ! cell+ ! ;

                colon   TWOSTORE,2,"2!"

                dw      SWAP,OVER,STORE,CELLPLUS,STORE,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; 2drop        x1 x2 --                Drop 2 cells
;;
;;      drop drop ;

                colon   TWODROP,5,"2drop"

                dw      DROP,DROP,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; 2dup         x1 x2 -- x1 x2 x1 x2    Duplicate top 2 cells
;;
;;      over over ;

                colon   TWODUP,4,"2dup"

                dw      OVER,OVER,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; 2swap        x1 x2 x3 x4 -- x3 x4 x1 x2      Swap 2 sets of 2 cells
;;
;;      rot >r rot r> ;

                colon   TWOSWAP,5,"2swap"

                dw      ROT,TOR,ROT,RFROM,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; 2over        x1 x2 x3 x4 -- x1 x2 x3 x4 x1 x2
;;
;;      >r >r 2dup r> r> 2swap ;

                colon   TWOOVER,5,"2over"

                dw      TOR,TOR,TWODUP,RFROM,RFROM,TWOSWAP,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; cr           --                              Output newline
;;
;;      13 emit ;

                colon   CR,2,"cr"

                dw      LIT,13,EMIT,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; count        addr -- addr u                  Read start of string and show address, length
;;
;;      dup char+ swap c@ ;

                colon   COUNT,5,"count"

                dw      DUP,CHARPLUS,SWAP,CFETCH,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; space        --                              Output a space
;;
;;      bl emit ;

                colon   SPACE,5,"space"

                dw      BL,EMIT,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; spaces       n --                            Output n spaces
;;
;;      begin dup while space 1- repeat drop ;

                colon   SPACES,5,"spaces"

.l1             dw      DUP,QBRANCH,.l2
                dw      SPACE,ONEMINUS,BRANCH,.l1
.l2              dw      DROP,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; umin         u1 u2 -- u3                     u3 = min(u1, u2)
;;
;;      2dup u> if swap then drop ;

                colon   UMIN,4,"umin"

                dw      TWODUP,UGREATER,QBRANCH,.l1,SWAP
.l1             dw      DROP,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; umax         u1 u2 -- u3                     u3 = max(u1, u2)
;;
;;      2dup u< if swap then drop ;

                colon   UMAX,4,"umax"

                dw      TWODUP,ULESS,QBRANCH,.l1,SWAP
.l1             dw      DROP,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; accept       addr +n -- +n'                  Get line from keyboard - return length
;;
;;      over + 1- over          -- start end addr
;;      begin key               -- start end addr key
;;      dup 13 <> while
;;              dup emit
;;              dup 10 = if                             Delete?
;;                      drop 1- >r over r> umax         Ensure you don't delete past beginning
;;              else
;;                      over c! 1+ over umin            Insert key but not past end
;;              then
;;      repeat                  -- start end addr key
;;      drop nip swap - ;       

                colon   ACCEPT,6,"accept"

                dw      OVER,PLUS,ONEMINUS,OVER
.l1             dw      KEY,DUP,LIT,13,NOTEQUAL,QBRANCH,.l4
                dw      DUP,EMIT,DUP,LIT,10,EQUAL,QBRANCH,.l2
                dw      DROP,ONEMINUS,TOR,OVER,RFROM,UMAX
                dw      BRANCH,.l3
.l2             dw      OVER,CSTORE,ONEPLUS,OVER,UMIN
.l3             dw      BRANCH,.l1
.l4             dw      DROP,NIP,SWAP,MINUS,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; type         addr +n --                      Type line to terminal
;;
;;      dup? if
;;              over + swap do i c@ emit loop
;;      else
;;              drop then ;

                colon   TYPE,4,"type"

                dw      QDUP,QBRANCH,.l2
                dw      OVER,PLUS,SWAP,XDO
.l1             dw      II,CFETCH,EMIT,XLOOP,.l1
                dw      BRANCH,.l3
.l2             dw      DROP
.l3             dw      EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; (s")         -- addr u                       Run time code for s"
;;
;;      r> count 2dup + aligned >r ;

                colon   XSQUOTE,4,"(s\")"

                dw      RFROM,COUNT,TWODUP,PLUS,ALIGNED,TOR,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; s"           --                              Compile inline string
;;
;;      compile (s") [ hex ]
;;      22 word c@ 1+ aligned allot ; immediate

                immed   SQUOTE,2,"s\""

                dw      LIT,XSQUOTE,COMMAXT
                dw      LIT,$22,WORD,CFETCH,ONEPLUS
                dw      ALIGNED,ALLOT,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; ."           --                              Compile string to print
;;
;;      postpone s" postpone type ; immediate

                immed   DOTQUOTE,2,".\""

                dw      SQUOTE
                dw      LIT,TYPE,COMMAXT
                dw      EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; ud/mod       ud1 u2 -- u3 ud4                32/16 -> 32,16 divide
;;
;;      >r 0 r@ um/mod rot rot r> um/mod ;

                colon   UDSLASHMOD,6,"ud/mod"

                dw      TOR,LIT,0,RFETCH,UMSLASHMOD,ROT,ROT,RFROM,UMSLASHMOD,ROT,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; ud*          ud1 u2 -- ud3                   32*16 -> 32 multiply
;;
;;      dup >r um* drop swap r> um* rot + ;

                colon   UDSTAR,3,"ud*"

                dw      DUP,TOR,UMSTAR,DROP,SWAP,RFROM,UMSTAR,ROT,PLUS,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; hold         c --                            Add char to output string
;;
;;      -1 hp +! hp @ C! ;

                colon   HOLD,4,"hold"

                dw      LIT,-1,HP,PLUSSTORE,HP,FETCH,CSTORE,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; <#           --                              Begin numeric conversion
;;
;;      pad hp ! ;              Initialise HOLD pointer

                colon   LESSNUM,2,"<#"

                dw      PAD,HP,STORE,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; >digit       n -- c                          Convert to 0..9A..Z

                head    TODIGIT,6,">digit",docode

                ld      a,c
                ld      b,0
                cp      10
                sbc     a,$69
                daa
                ld      c,a
                next

;;----------------------------------------------------------------------------------------------------------------------
;; #            ud1 -- ud2                      Convert 1 digit of output
;;
;;      base @ ud/mod rot >digit hold ;

                colon   NUM,1,"#"

                dw      BASE,FETCH,UDSLASHMOD,ROT,TODIGIT,HOLD,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; #s           ud1 -- ud2                      Convert remain digits
;;
;;      begin # 2dup or 0= until ;

                colon   NUMS,2,"#s"

.l1             dw      NUM,TWODUP,OR,ZEROEQUAL,QBRANCH,.l1,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; #>           ud1 -- addr u                   End conversion, get string
;;
;;      2drop hp @ pad over - ;

                colon   NUMGREATER,2,"#>"

                dw      TWODROP,HP,FETCH,PAD,OVER,MINUS,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; sign         n --                            Add minus sign if n < 0
;;
;;      0< if $2d HOLD THEN ;

                colon   SIGN,4,"sign"

                dw      ZEROLESS,QBRANCH,.l1,LIT,$2d,HOLD
.l1             dw      EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; u.           u --                            Display u unsigned
;;
;;      <# 0 #s #> type space ;

                colon   UDOT,2,"u."

                dw      LESSNUM,LIT,0,NUMS,NUMGREATER,TYPE,SPACE,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; .            n --                            Display n signed
;;
;;      <# dup abs 0 #s rot sign #> type space ;

                colon   DOT,1,"."

                dw      LESSNUM,DUP,ABS,LIT,0,NUMS,ROT,SIGN,NUMGREATER,TYPE,SPACE,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; decimal      --                              Set number base to decimal
;;
;;      10 base ! ;

                colon   DECIMAL,7,"decimal"

                dw      LIT,10,BASE,STORE,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; hex          --                              Set number base to hex
;;
;;      16 base ! ;

                colon   HEX,3,"hex"

                dw      LIT,16,BASE,STORE,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; here         -- addr                         Returns dictionary pointer
;;
;;      DP @ ;

                colon   HERE,4,"here"

                dw      DP,FETCH,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; allot        n --                            Allocate n bytes in dictionary
;;
;;      DP +! ;

                colon   ALLOT,5,"allot"

                dw      DP,PLUSSTORE,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; ,            x --                            Append cell to dictionary
;;
;;      HERE ! 1 CELLS ALLOT ;

                colon   COMMA,1,","

                dw      HERE,STORE,LIT,1,CELLS,ALLOT,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; c,           c --                            Append char to dict
;;
;;      HERE ! 1 CHARS ALLOT ;

                colon   CCOMMA,2,"c,"

                dw      HERE,STORE,LIT,1,CHARS,ALLOT,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; source       -- addr n                       Current input buffer
;;
;;      'source 2@ ;

                colon   SOURCE,6,"source"

                dw      TICKSOURCE,TWOFETCH,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; /string      addr len n -- addr+n u-n          Trim string (remove first n chars)
;;
;;      rot over + rot rot - ;

                colon   SLASHSTRING,7,"/string"

                dw      ROT,OVER,PLUS,ROT,ROT,MINUS,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; >counted     src n dst --                    Copy to counted string
;;
;;      2dup c! char+ swap cmove ;

                colon   TOCOUNTED,8,">counted"

                dw      TWODUP,CSTORE,CHARPLUS,SWAP,CMOVE,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; word         char -- addr n                  Word delimited by char
;;
;;      dup source >in @ /string        -- c c addr n
;;      dup >r rot skip                 -- c addr' n' | n
;;      over >r rot scan                -- addr" n" | n addr'
;;      dup if char- then               Skip trailing delimiter
;;      r> r> rot - >in +!              Update >IN offset
;;      tuck -
;;      here >counted
;;      here
;;      bl over count + c! ;            Append trailing blank

                colon   WORD,4,"word"

                dw      DUP,SOURCE,TOIN,FETCH,SLASHSTRING
                dw      DUP,TOR,ROT,SKIP
                dw      OVER,TOR,ROT,SCAN
                dw      DUP,QBRANCH,.l1,ONEMINUS
.l1             dw      RFROM,RFROM,ROT,MINUS,TOIN,PLUSSTORE
                dw      TUCK,MINUS
                dw      HERE,TOCOUNTED,HERE
                dw      BL,OVER,COUNT,PLUS,CSTORE,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; nfa>lfa      nfa -- lfa                      Convert name address to link field
;;
;;      3 - ;

                colon   NFATOLFA,7,"nfa>lfa"

                dw      LIT,3,MINUS,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; nfa>cfa      nfa -- cfa                      Convert name address to code field
;;
;;      count $7f and + ;               Mask of smudge bit

                colon   NFATOCFA,7,"nfa>cfa"

                dw      COUNT,LIT,$7f,AND,PLUS,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; immed?       nfa -- f                        Fetch immediate flag
;;
;;      1- c@ ;

                colon   IMMEDQ,6,"immed?"

                dw      ONEMINUS,CFETCH,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; find         addr -- addr 0                  If not found
;;                      xt 1                    If immediate
;;                      xt -1                   If normal
;;
;;      latest @ begin                  -- addr nfa
;;              2dup over c@ char+      Get length of string    -- addr nfa addr nfa len+1
;;              s=                      Compare strings         -- addr nfa
;;              dup if                  Not reached end?
;;                      drop                                    -- addr nfa
;;                      nfa>lfa @ dup   Get link addr           -- addr nfa' nfa'
;;              then
;;      0= until                        -- addr nfa OR addr 0
;;      dup if                          Found?
;;              nip dup nfa>cfa         -- nfa cfa
;;              swap immed?             -- cfa immed?
;;              0= 1 or                 -- cfa 1/-1
;;      then ;

                colon   FIND,4,"find"

                dw      LATEST,FETCH
.l1             dw      TWODUP,OVER,CFETCH,CHARPLUS
                dw      SEQUAL,DUP,QBRANCH,.l2
                dw      DROP,NFATOLFA,FETCH,DUP
.l2             dw      ZEROEQUAL,QBRANCH,.l1
                dw      DUP,QBRANCH,.l3
                dw      NIP,DUP,NFATOCFA
                dw      SWAP,IMMEDQ,ZEROEQUAL,LIT,1,OR
.l3             dw      EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; literal      x --                            Append numeric literal
;;
;;      state @ if ['] lit ,xt , then ; immediate

                immed   LITERAL,7,"literal"

                dw      STATE,FETCH,QBRANCH,.l1
                dw      LIT,LIT,COMMAXT,COMMA
.l1             dw      EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; digit?       c -- n -1                       If c is a valid digit
;;                   x 0                        Otherwise
;;
;;      [ hex ] dup 39 > 100 and +
;;      dup 140 > 107 and - 30 -
;;      dup base @ u< ;

                colon   DIGITQ,6,"digit?"

                dw      DUP,LIT,$39,GREATER,LIT,$100,AND,PLUS
                dw      DUP,LIT,$160,GREATER,LIT,$127,AND
                dw      MINUS,LIT,$30,MINUS
                dw      DUP,BASE,FETCH,ULESS,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; ?sign        addr n -- addr' n' f            Get optional sign
;;
;;      over c@                         -- addr n c
;;      $2c - dup abs 1 = and           -- -1/+1/0      -1 = +, _1 = -, or 0 otherwise
;;      dup if 1+                       -- 0/2          If signed
;;              >r 1 /string r>         -- addr' n' f
;;      then ;

                colon   QSIGN,5,"?sign"

                dw      OVER,CFETCH,LIT,$2c,MINUS,DUP,ABS,LIT,1,EQUAL,AND
                dw      DUP,QBRANCH,.l1,ONEPLUS
                dw      TOR,LIT,1,SLASHSTRING,RFROM
.l1             dw      EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; >number      ud addr u -- ud' addr' u'       Convert string to number
;;
;;      begin
;;      dup while
;;              over c@ digit?
;;              0= if drop exit then    End of numeric string
;;              >r 2swap base @ ud*     Update total
;;              r> m+ 2swap
;;              1 /string
;;      repeat ;

                colon TONUMBER,7,">number"

.l1             dw      DUP,QBRANCH,.l3
                dw      OVER,CFETCH,DIGITQ
                dw      ZEROEQUAL,QBRANCH,.l2,DROP,EXIT
.l2             dw      TOR,TWOSWAP,BASE,FETCH,UDSTAR
                dw      RFROM,MPLUS,TWOSWAP
                dw      LIT,1,SLASHSTRING,BRANCH,.l1
.l3             dw      EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; ?number      addr -- n -1                    String -> number
;;                      addr 0               If conversion error occurs
;;
;;      dup 0 0 rot count               -- addr 0(d) addr+1 len
;;      ?sign >r >number                -- addr n(d) addr' len' | sign?
;;      if r> 2drop 2drop 0             -- addr 0
;;      else 2drop nip r>               -- n(d) sign?
;;              if negate then -1       -- n -1
;;      then;

                colon   QNUMBER,7,"?number"

                dw      DUP,LIT,0,DUP,ROT,COUNT
                dw      QSIGN,TOR,TONUMBER,QBRANCH,.l1
                dw      RFROM,TWODROP,TWODROP,LIT,0
                dw      BRANCH,.l3
.l1             dw      TWODROP,NIP,RFROM,QBRANCH,.l2,NEGATE
.l2             dw      LIT,-1
.l3             dw      EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; interpret    i*x addr len -- j*x             Interpret given buffer
;;
;;      'source 2! 0 >IN !
;;      begin
;;      bl word dup c@ while                    -- textaddr
;;              find                            -- a 0/1/-1
;;              ?dup if                         -- xt 1/-1
;;                      1+ state @ 0= or        Immediate or interpreted?
;;                      if execute else ,xt then
;;              else
;;                      ?number
;;                      if postpone literal
;;                      else count type $3f emit cr abort
;;                      then
;;              then
;;      repeat drop ;

                colon   INTERPRET,9,"interpret"

                dw      TICKSOURCE,TWOSTORE,LIT,0,TOIN,STORE
.l1             dw      BL,WORD,DUP,CFETCH,QBRANCH,.l9
                dw      FIND,QDUP,QBRANCH,.l4
                dw      ONEPLUS,STATE,FETCH,ZEROEQUAL,OR
                dw      QBRANCH,.l2
                dw      EXECUTE,BRANCH,.l3
.l2             dw      COMMAXT
.l3             dw      BRANCH,.l8
.l4             dw      QNUMBER,QBRANCH,.l5
                dw      LITERAL,BRANCH,.l6
.l5             dw      COUNT,TYPE,LIT,$3f,EMIT,CR,ABORT
.l6
.l8             dw      BRANCH,.l1
.l9             dw      DROP,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; evaluate     i*x addr len -- j*x             Interpret string
;;
;;      'source 2@ >r >r >in @ >r
;;      interpret
;;      r> >in ! r> r> 'source 2! ;

                colon   EVALUATE,8,"evaluate"

                dw      TICKSOURCE,TWOFETCH,TOR,TOR,TOIN,FETCH,TOR,INTERPRET
                dw      RFROM,TOIN,STORE,RFROM,RFROM,TICKSOURCE,TWOSTORE,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; quit         --      R: i*x --               Interpret from keyboard
;;
;;      l0 lp ! r0 rp! 0 state !
;;      begin
;;              tib dup tibsize accept space
;;              interpret
;;              state @ 0= if cr ." OK" then
;;      again ;

                colon   QUIT,4,"quit"

                dw      L0,LP,STORE
                dw      R0,RPSTORE,LIT,0,STATE,STORE
.l1             dw      TIB,DUP,TIBSIZE,ACCEPT,SPACE
                dw      INTERPRET
                dw      STATE,FETCH,ZEROEQUAL,QBRANCH,.l2
                dw      CR,XSQUOTE
                db      3,"OK",13
                dw      TYPE
.l2             dw      BRANCH,.l1

;;----------------------------------------------------------------------------------------------------------------------
;; abort        i*x --          R: j*x --       Clear stack and quit
;;
;;      s0 sp! quit ;

                colon   ABORT,5,"abort"

                dw      S0,SPSTORE,QUIT

;;----------------------------------------------------------------------------------------------------------------------
;; ?abort       f addr len --                   Abort and print message
;;
;;      rot if type abort then 2drop ;

                colon   QABORT,6,"?abort"

                dw      ROT,QBRANCH,.l1,TYPE,ABORT
.l1             dw      TWODROP,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; abort"       i*x x1 -- i*x   R: j*x -- j*x   x1 = 0
;;              i*x x1 --       R: j*x --       x1 != 0
;;
;;      postpone s" postpone ?abort ; immediate

                immed   ABORTQUOTE,6,"abort\""

                dw      SQUOTE,LIT,QABORT,COMMAXT,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; '            -- xt                           Find word in dictionary
;;
;;      bl word find
;;      0= abort" ?" ;

                colon   TICK,1,"'"

                dw      BL,WORD,FIND,ZEROEQUAL,XSQUOTE
                db      1,"?"
                dw      QABORT,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; char         -- char                         parse ASCII character
;;
;;      bl word 1+ c@ ;

                colon   CHAR,4,"char"

                dw      BL,WORD,ONEPLUS,CFETCH,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; [char]       --                              Compile character literal
;;
;;      char ['] lit ,xt , ; immediate

                immed   BRACCHAR,6,"[char]"

                dw      CHAR
                dw      LIT,LIT,COMMAXT
                dw      COMMA,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; (            --                              Skip until )
;;
;;      [ hex ] 29 word drop ; immediate

                immed   PAREN,1,"("

                dw      LIT,$29,WORD,DROP,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; create       --                              Create an empty definition
;;
;;      latest @ , 0 c,                 Link & immediate field
;;      here latest !                   New latest link
;;      bl word c@ 1+ allot             Name field
;;      docreate ,cf ;                  Code field

                colon   CREATE,6,"create"

                dw      LATEST,FETCH,COMMA,LIT,0,CCOMMA
                dw      HERE,LATEST,STORE
                dw      BL,WORD,CFETCH,ONEPLUS,ALLOT
                dw      LIT,docreate,COMMACF,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; (does>)      --                              Runtime action of DOES>
;;
;;      r>
;;      latest @ nfa>cfa
;;      !cf ;

                colon   XDOES,7,"(does>)"

                dw      RFROM,LATEST,FETCH,NFATOCFA,STORECF,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; does>        --                              Change action of latest definition
;;
;;      compile (does>)
;;      <dodoes> ,CF ; immediate

                immed   DOES,5,"does>"

                dw      LIT,XDOES,COMMAXT,LIT,dodoes,COMMACF,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; recurse      --                              Recurse current definition
;;
;;      latest @ nfa>cfa ,xt ; immediate

                immed   RECURSE,7,"recurse"

                dw      LATEST,FETCH,NFATOCFA,COMMAXT,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; [            --                              Enter interpretive state
;;
;;      0 state ! ; immediate

                immed   LEFTBRACKET,1,"["

                dw      LIT,0,STATE,STORE,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; ]            --                              Enter compiling state
;;
;;      -1 state ! ; immediate

                colon   RIGHTBRACKET,1,"]"

                dw      LIT,-1,STATE,STORE,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; hide         --                              Hide latest definition
;;
;;      latest @ dup c@ $80 or swap c! ;

                colon   HIDE,4,"hide"

                dw      LATEST,FETCH,DUP,CFETCH,LIT,$80,OR,SWAP,CSTORE,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; reveal       --                              Reveal latest defintion
;;
;;      latest @ dup c@ $7f and swap c! ;

                colon   REVEAL,6,"reveal"

                dw      LATEST,FETCH,DUP,CFETCH,LIT,$7f,AND,SWAP,CSTORE,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; immediate    --                              Make last definition immediate
;;
;;      1 latest @ 1- c! ;

                colon   IMMEDIATE,9,"immediate"

                dw      LIT,1,LATEST,FETCH,ONEMINUS,CSTORE,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; :            --                              Begin a colon definition
;;
;;      create hide ] !colon ;

                head    COLON,1,":",docode

                call    ENTER                   ; Code fwd ref explicitly
                dw      CREATE,HIDE,RIGHTBRACKET
                dw      STORECOLON,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; ;            --                              End a colon definition
;;
;;      reveal ,exit postpone [ ; immediate

                immed   SEMICOLON,1,";"

                dw      REVEAL,COMMAEXIT,LEFTBRACKET,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; [']          --                              Find a word & compile as literal
;;
;;      ' lit lit ,xt , ; immediate

                immed   BRACTICK,3,"[']"

                dw      TICK,LIT,LIT,COMMAXT,COMMA,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; postpone     --                              Postpone compile action of word
;;
;;      bl word find
;;      dup 0= abort" ?"
;;      0< if
;;              ['] lit ,xt ,
;;              ['] ,xt ,xt
;;      else ,xt
;;      then ; immediate

                immed POSTPONE,8,"postpone"

                dw      BL,WORD,FIND,DUP,ZEROEQUAL,XSQUOTE
                db      1,"?"
                dw      QABORT,ZEROLESS,QBRANCH,.l1
                dw      LIT,LIT,COMMAXT,COMMA
                dw      LIT,COMMAXT,COMMAXT,BRANCH,.l2
.l1             dw      COMMAXT
.l2             dw      EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; if           -- addr                         Conditional forward branch
;;
;;      ['] qbranch ,branch here dup ,dest ; immediate

                immed   IF,2,"if"

                dw      LIT,QBRANCH,COMMABRANCH,HERE,DUP,COMMADEST,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; then         addr --                         Resolve forward branch
;;
;;      here swap !dest ; immediate

                immed   THEN,4,"then"

                dw      HERE,SWAP,STOREDEST,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; else         addr1 -- addr2                  Branch for IF..ELSE
;;
;;      ['] branch ,branch here dup ,dest
;;      swap postpone then ; immediate

                immed   ELSE,4,"else"

                dw      LIT,BRANCH,COMMABRANCH,HERE,DUP,COMMADEST,SWAP,THEN,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; begin        -- addr                         Target for backward branch
;;
;;      here ; immediate

                cimmed  BEGIN,5,"begin"

                jp      HERE

;;----------------------------------------------------------------------------------------------------------------------
;; until        addr --                         Conditional backward branch
;;
;;      ['] qbranch ,branch ,dest ; immediate

                immed   UNTIL,5,"until"

                dw      LIT,QBRANCH,COMMABRANCH,COMMADEST,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; again        addr --                         Unconditional backward branch
;;
;;      ['] branch ,branch ,dest ; immediate

                immed   AGAIN,5,"again"

                dw      LIT,BRANCH,COMMABRANCH,COMMADEST,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; while        -- adde                         Branch for WHILE loop
;;
;;      postpone if ; immediate

                cimmed  WHILE,5,"while"

                jp      IF

;;----------------------------------------------------------------------------------------------------------------------
;; repeat       addr1 addr2 --                  Resolve WHILE loop
;;
;;      swap postpone again postpone then ; immediate

                immed REPEAT,6,"repeat"

                dw      SWAP,AGAIN,THEN,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; >l          x --    L: -- x                 Move to leave stack
;;
;;      cell lp +! lp @ ! ;

                colon   TOL,2,">l"

                dw      CELL,LP,PLUSSTORE,LP,FETCH,STORE,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; l>           -- x    L: x --                 Move from leave stack
;;
;;      lp @ @ cell negate lp +! ;

                colon   LFROM,2,"l>"

                dw      LP,FETCH,FETCH,CELL,NEGATE,LP,PLUSSTORE,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; do           -- addr         L: -- 0
;;
;;      ['] xdo ,xt here        Target for backward branch
;;      0 >l ; immediate        Marker for leaves

                immed   DO,2,"do"

                dw      LIT,XDO,COMMAXT,HERE,LIT,0,TOL,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; endloop      addr xt --      L: -- 0 a1 a2 .. aN --
;;
;;      ,branch ,dest                           Backward loop
;;      begin l> ?dup while postpone then repeat ;

                colon   ENDLOOP,7,"endloop"

                dw      COMMABRANCH,COMMADEST
.l1             dw      LFROM,QDUP,QBRANCH,.l2
                dw      THEN,BRANCH,.l1
.l2             dw      EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; loop         addr --         L: 0 a1 a2 .. aN --
;;
;;      ['] xloop endloop ; immediate

                immed   LOOP,4,"loop"

                dw      LIT,XLOOP,ENDLOOP,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; +loop        addr --         L: 0 a1 a2 .. aN --
;;
;;      ['] xloop endloop ; immediate

                immed   PLUSLOOP,4,"+loop"

                dw      LIT,XPLUSLOOP,ENDLOOP,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; leave        --              L: -- addr
;;
;;      ['] unloop ,xt
;;      ['] branch ,branch here dup ,dest >L ; immediate

                immed   LEAVE,5,"leave"

                dw      LIT,UNLOOP,COMMAXT
                dw      LIT,BRANCH,COMMABRANCH
                dw      HERE,DUP,COMMADEST,TOL,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; within       n1|u1 n2|u2 n3|u3 -- f          n2 <= n1 < n3?
;;
;;      over - >r - r> u< ;

                colon   WITHIN,6,"within"

                dw      OVER,MINUS,TOR,MINUS,RFROM,ULESS,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; move         addr1 addr2 u --                Smart move
;;
;;      r> 2dup swap dup r@ +
;;      within if r> cmove>
;;      else r> cmove then ;

                colon   MOVE,4,"move"

                dw      TOR,TWODUP,SWAP,DUP,RFETCH,PLUS
                dw      WITHIN,QBRANCH,.move1
                dw      RFROM,CMOVEUP,BRANCH,.move2
.move1          dw      RFROM,CMOVE
.move2          dw      EXIT


;;----------------------------------------------------------------------------------------------------------------------
;; depth        -- +n                           Number of items on stack
;;
;;      sp@ s0 swap - 2/ ;

                colon   DEPTH,5,"depth"

                dw      SPFETCH,S0,SWAP,MINUS,TWOSLASH,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; words        --                              List all words in dict
;;
;;      latest @ begin
;;              dup count type space
;;              nfa>lfa @
;;      dup 0= until
;;      drop ;

                colon   WORDS,5,"words"

                dw      LATEST,FETCH
.l1             dw      DUP,COUNT,TYPE,SPACE,NFATOLFA,FETCH
                dw      DUP,ZEROEQUAL,QBRANCH,.l1
                dw      DROP,EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; .s           --                              Print stack contents
;;
;;      sp@ s0 - if
;;              sp@ s0 2 - do i @ u. -2 +loop
;;      then ;

                colon   DOTS,2,".s"

                dw      SPFETCH,S0,MINUS,QBRANCH,.l2
                dw      SPFETCH,S0,LIT,2,MINUS,XDO
.l1             dw      II,FETCH,UDOT,LIT,-2,XPLUSLOOP,.l1
.l2             dw      EXIT

;;----------------------------------------------------------------------------------------------------------------------
;; cold         --                              Cold start forth system
;;
;;      uinit u0 #init cmove cls
;;      ." Next Forth (v0.1)" 13 emit
;;      abort ;

                head    COLD,4,"cold",docode

                ld      sp,$3ffe
                call    textMode
                call    cursorOn
                call    ENTER

                dw      UINIT,U0,NINIT,CMOVE,CLS
                dw      XSQUOTE
                db      19,"Next Forth (v0.1)",13,13
                dw      TYPE,ABORT

;;----------------------------------------------------------------------------------------------------------------------
;; brk          --                              Causes Cspect to break
;;

                head    BRK,3,"brk",docode

                break
                next

;;----------------------------------------------------------------------------------------------------------------------
;; cls          --                              Clear the console screen

                head    CLS,3,"cls",docode

                push    bc
                push    de
                call    cls
                pop     de
                pop     bc
                next

;;----------------------------------------------------------------------------------------------------------------------
;; textmode     --                              Tilemap-based text mode

                head    TEXTMODE,8,"textmode",docode

                push    bc
                push    de
                call    textMode
                pop     de
                pop     bc
                next

;;----------------------------------------------------------------------------------------------------------------------
;; ulamode      --                              Normal ULA mode

                head    ULAMODE,7,"ulamode",docode

                push    bc
                push    de
                call    ulaMode
                pop     de
                pop     bc
                next


;;----------------------------------------------------------------------------------------------------------------------

LASTWORD        equ     last_link
