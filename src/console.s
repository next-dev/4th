;;----------------------------------------------------------------------------------------------------------------------
;; Console emulation
;;
;; Routines:
;;
;;      ulaMode         switch to ULA mode
;;      textMode        switch to text mode (special use of tilemaps)
;;      at              Move the cursor to a certain position
;;      cursorOn        Turn on the cursor
;;      cursorOff       Turn off the cursor
;;      consoleUpdate   Update cursor
;;      print           Print a message
;;      cls             Clear the screen
;;      input           Fetch up to 79 characters on a row
;;      newline         Go to the next row
;;      setColour       Set colour for text colour slot n
;;      setInk          Set the colour slot for the next text printed

;;----------------------------------------------------------------------------------------------------------------------
;; initConsole
;; Allocates a page for storing $6000-$7fff

ConsolePage     db      0

initConsole:
                call    allocPage
                ld      (ConsolePage),a

                nextreg $56,a           ; Page in new page into MMU 6
                ld      hl,$6000
                ld      de,$c000
                ld      bc,$2000
                call    memcpy          ; Back up page 11
                nextreg $56,0           ; Restore MMU 6

                ; Generate font
                ld      hl,Font
                ld      de,$6000
                ld      bc,FontSize

                ; Generate it from smaller data
.l1             push    bc
                ld      b,4
                ld      c,(hl)          ; Get byte
                inc     hl
.l2             xor     a
                sla     c               ; Get pixel
                rla                     ; Bring pixel in
                swapnib
                srl     a
                sla     c               ; Get next pixel
                rla                     ; Bring in other pixel
                ld      (de),a
                inc     de
                djnz    .l2
                pop     bc
                dec     bc
                ld      a,b
                or      c
                jr      nz,.l1

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; doneConsole
;; Restore page 11

doneConsole:
                call    ulaMode
                ld      a,(ConsolePage)
                nextreg $56,a
                ld      hl,$c000
                ld      de,$6000
                ld      bc,$2000
                call    memcpy
                nextreg $56,0
                ld      a,(ConsolePage)
                call    freePage

                ret


;;----------------------------------------------------------------------------------------------------------------------
;; ulaMode

ulaMode:
                xor     a
                nextreg $68,a           ; Enable ULA
                nextreg $6b,0           ; Disable tilemap

                ld      hl,$4000
                ld      bc,6144
                call    memfill
                ld      a,($5c8d)       ; Get permanent attr
                ld      hl,$5800
                ld      bc,768
                call    memfill

                ld      a,($5c48)       ; Get border colour
                rrca
                rrca
                rrca                    ; Get the colour field
                out     (IO_ULA),a

                call    cursorOff

                ret

;;----------------------------------------------------------------------------------------------------------------------

textMode:
                xor     a
                out     (IO_ULA),a

        ; Initialise palette
        ;
        ; 0 = Normal text
        ; 1 = Cursor
        ; 2 = Inverse text
        ; 3 = Error text
        ; 4 = Input text

                ld      bc,$0007        ; Slot 0 = Black/White
                call    setColour
                ld      bc,$090f        ; Slot 1 = Bright Blue/Bright White
                call    setColour
                ld      bc,$0700        ; Slot 2 = White/Black
                call    setColour
                ld      bc,$0002        ; Slot 3 = Black/Red
                call    setColour
                ld      bc,$000f        ; Slot 4 = Black/Bright White
                call    setColour

                nextreg $6b,%11000001   ; Tilemap control
                nextreg $6e,$00         ; Tilemap base offset
                nextreg $6f,$20         ; Tiles base offset
                nextreg $4c,8           ; Transparency colour (bright black)
                nextreg $68,%10000000   ; Disable ULA output

                ; Reset scrolling and clip window
                xor     a
                nextreg $1b,a
                nextreg $1b,159
                nextreg $1b,a
                nextreg $1b,255         ; Reset clip window
                nextreg $2f,a
                nextreg $30,a
                nextreg $31,a           ; Reset scrolling
                call    cls
                call    cursorOn

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Palette control

Palette:
                db  %00000000
                db  %00000010
                db  %10100000
                db  %10100010
                db  %00010100
                db  %00010110
                db  %10110100
                db  %10110110
                db  %01101101
                db  %00000011
                db  %11100000
                db  %11100011
                db  %00011100
                db  %00011111
                db  %11111100
                db  %11111111

setColour:
        ; Input:
        ;       B = Paper
        ;       C = Ink
        ;       A = Slot (0-15)
        ; Ouput:
        ;       A = Next slot
        ; Uses:
        ;       HL, DE, BC
                push    af
                nextreg $43,%00110000   ; Set tilemap palette
                sla     a
                sla     a
                sla     a
                sla     a
                nextreg $40,a
                ; Paper colour
                ld      de,Palette
                ld      l,b
                ld      h,0
                add     hl,de
                ld      a,(hl)
                nextreg $41,a
                ; Ink colour
                ld      de,Palette
                ld      l,c
                ld      h,0
                add     hl,de
                ld      a,(hl)
                nextreg $41,a
                pop     af
                inc     a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cls
;; Clears the screen

cls:
        ; todo - set char 0 as blank to and use DMA to fill the tilemap
        ; Clear screen (write spaces in colour 0 everywhere)
        ; Uses:
        ;       BC, HL, A
        ;
                ld      hl,$4000
                ld      bc,5120
                xor     a
                call    memfill
                ld      bc,0
                call    at
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; at
;; Sets the cursor position

CurrentCoords   dw      0
CurrentPos:     dw      0

at:
        ; Input:
        ;       BC = XY coord
        ;
        ; Output:
        ;       HL = tile address
        ;       CurrentPos set to tile address too
        ;       A = X
        ;
                ld      (CurrentCoords),bc
                push    de
                ld      e,c
                ld      d,80
                mul                     ; DE = 80Y
                ex      de,hl
                ld      a,b
                add     hl,a            ; HL = 80Y + X
                add     hl,hl           ;    = (80Y + X) * 2
                ld      de,$4000
                add     hl,de           ; Current position
                ld      (CurrentPos),hl
                pop     de
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; print
;; Print the text at DE

CurrentInk      dw      0

SpaceString     db      "  ",0

print:
        ; todo - speed this up (perhaps not updating coords every character)
        ; Input:
        ;       DE = text
        ;
                push    bc
                push    de
                push    hl
                ld      hl,(CurrentPos)
                ld      a,(CurrentInk)
                swapnib
                ld      c,a             ; C = tilemap attribute
.l1
                ; Test to see if we're outside the screen
                ld      a,h
                cp      $54             ; Reached end of screen ($5400 is past end of screen)?
                jr      nz,.cont

                ; Reached end of screen
                call    scroll
                jr      .l1             ; Keep scrolling until we're in screen

.cont           ld      a,(de)          ; Fetch character
                and     a
                inc     de              ; Next character pointer
                jr      z,.finish       ; End of string?
                cp      10              ; Delete?
                jr      z,.delete
                cp      13
                jr      nz,.not_cr      ; Carriage return

                ; Go to next line
                push    bc
                ld      bc,(CurrentCoords)
.nextline       inc     c
                ld      b,0
                call    at
                jr      .next_char

.delete         push    bc
                ld      bc,(CurrentCoords)
                ld      a,b
                and     a               ; At beginnng of line?
                jr      nz,.no_beg
                ld      a,c
                and     a               ; At beginning of screen?
                jr      z,.next_char
                dec     c
                ld      b,81
.no_beg         dec     b

                push    de
                call    at              ; Set up position for sub-printing
                ld      de,SpaceString  ; Print space over the character we've deleted
                call    print
                pop     de
                call    at              ; Restore position

                call    at
                jr      .next_char

.not_cr
                ld      (hl),a          ; Write out character
                inc     hl
                ld      (hl),c          ; Write out attribute
                inc     hl

                ; Update coords
                push    bc
                ld      bc,(CurrentCoords)
                inc     b
                ld      a,b
                cp      80
                jr      z,.nextline

                ; Go to next line
.next_char      ld      (CurrentCoords),bc
                pop     bc
                jr      .l1
                
.finish:
                ld      (CurrentPos),hl
                pop     hl
                pop     de
                pop     bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; printChar

CharBuffer:     db      0,0

printChar:
        ; Input:
        ;       A = character
        ;
                push    de
                ld      (CharBuffer),a
                ld      de,CharBuffer
                call    print
                pop     de
                ret


;;----------------------------------------------------------------------------------------------------------------------
;; Cursor control

cursorState     db      0

cursorOn:
                ld      a,1
                ld      (cursorState),a
                ret

cursorOff:
                push    hl
                ld      hl,(CurrentPos)
                inc     hl
                ld      a,(CurrentInk)
                ld      (hl),a
                xor     a
                ld      (cursorState),a
                pop     hl
                ret

consoleUpdate:
                ;halt
                ld      a,(cursorState)
                and     a
                ret     z
                push    hl
                ld      hl,(CurrentPos)
                inc     hl
                ld      a,(Counter)
                and     16
                jr      z,.off
                jr      .cont
.off            ld      a,(CurrentInk)
                swapnib
.cont
                ld      (hl),a
                pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Scroll

scroll:
                push    af
                push    bc
                push    de
                halt

                ; Move tiles up wards
                ld      hl,$4000+(80*2)         ; Point to 2nd line
                ld      de,$4000                ; Move to 1st line
                ld      bc,80*31*2              ; Move 31 lines
                call    memcpy
                ld      hl,$4000+(31*80*2)      ; HL = last line
                ld      bc,160
                xor     a
                call    memfill

                ld      bc,(CurrentCoords)
                ld      a,c
                and     a                       ; Y == 0?
                jr      z,.no_adjust
                dec     c
                ld      (CurrentCoords),bc

.no_adjust      call    at                      ; HL = tilemap address
                pop     de
                pop     bc
                pop     af
                ret

