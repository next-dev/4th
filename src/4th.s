;;----------------------------------------------------------------------------------------------------------------------
;; ZX Spectrum Next Forth system
;;----------------------------------------------------------------------------------------------------------------------

                DEVICE ZXSPECTRUMNEXT
                CSPECTMAP "4th.map"

;;----------------------------------------------------------------------------------------------------------------------
;; Memory Map

;; 0000         Primitive routines and dictionary (loaded )

;; 2000         InitKeys, DoneKeys and initial jump
;; 2100         IM table
;; 2201         Code
;;
;; 3e00         Keyboard buffer
;; 3f00         Stack


;;----------------------------------------------------------------------------------------------------------------------


                include "src/z80n.s"
                include "src/keyboard.s"
                include "src/utils.s"
                include "src/filesys.s"
                include "src/memory.s"
                include "src/console.s"

                include "src/dict.s"

;;----------------------------------------------------------------------------------------------------------------------

PrimitivePages  dw      0
SystemPages     dw      0
OldSP           dw      $4000
OldIY           dw      0
Page0           db      0
StackPage       db      0

Start:
                ; Store the old stack pointer
                ld      hl,0
                add     hl,sp
                ld      (OldSP),hl
                call    allocPage               ; A = new page
                ld      (StackPage),a
                ld      sp,$4000

                ; Set up second dictionary
                ; Allocate pages and set them up for $c000+
                call    allocPage
                ld      (Page0),a
                nextreg $54,a

                ; Load data from dot file into $c000
                ld      hl,$8000
                ld      a,$ff
                ld      (hl),a
                rst     8
                db      $8d             ; M_GETHANDLE
                ld      ix,$8000
                ld      bc,$2000        ; 8K
                rst     8
                db      $9d             ; F_READ
                nextreg $54,4
                nextreg $55,5

                ; Set up the subsystems (keyboard and console)
                call    initKeys
                call    initConsole

                ld      (OldIY),iy
                ld      iy,UserBuffer

                ; Initialise Forth
                ld      a,(Page0)
                nextreg $50,a
                jp      COLD


                ; Close everything
endForth:
                nextreg $50,$ff
                nextreg $51,$ff
                ld      iy,(OldIY)
                ;ld      sp,0
                ld      sp,$4000
                call    doneConsole
                call    doneKeys

                ; Restore stack
                ld      hl,(OldSP)
                ld      sp,hl                   ; Restore stack pointer
                nextreg $57,1                   ; Restore page 1 into MMU 7
                ld      a,(StackPage)
                call    freePage
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Text font

Font:           incbin  "data/font.bin"
FontSize        equ     $-Font

;;----------------------------------------------------------------------------------------------------------------------

                display "Final address (1) (last address = 0x3E00): ",$

;;----------------------------------------------------------------------------------------------------------------------
;; Stack and keyboard buffer

                ds      $3e00 - $

KeyboardBuffer: ds      256
ParamStack:     ds      256
EndParamStack   equ     $

                display "End of part 1 (should be 0x4000): ", $

;;----------------------------------------------------------------------------------------------------------------------
;; Primitives


                org     $0000

Primitives:
                include "src/dict2.s"

                display "Final address (2) (last address = 0x1d00): ",$

                ds      $1d00 - $

UserBuffer:     ds      256             ; $1d00
TibBuffer:      ds      128             ; $1e00
HoldBuffer:     ds      40              ; $1e80
PadBuffer:      ds      88              ; $1ea8
ReturnStack:    ds      256             ; $1f00

                display "End of part 2 (should be 0x2000): ", $

;;----------------------------------------------------------------------------------------------------------------------
;; System code

                org     $8000
                ;incbin  "src/system.4th"
                ;ds      $c000 - $
                ;display "End of part 3 (should be 0xC000): ", $

;;----------------------------------------------------------------------------------------------------------------------
;; Binary generation

                SAVEBIN "f1",$2000,$2000
                SAVEBIN "f2",$0000,$2000
                ;SAVEBIN "f3",$8000,$4000

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
